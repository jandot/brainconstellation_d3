var svg = d3.select("body").append("svg").attr("width",2000).attr("height",600)

var sampleNames = ['1X4I9DF0','3E6J9LC6','3G0NTW65','3UBD8C14','4805PKZE','5X793NMC','5YR3OZ01','75S3EDZ5','7BP3W5Q1','85B4HLS7','9F6A8DQ2','AV823AV0','Q7C341EB','QH028UG1','R173GE0A','SVP271T6','SW1M13E7','U050E1IO']
var nrColumns = 6
var nrPixelsPerIndividual = 150

var cutoff = 0.2

var cutoffText = svg.append("text")
                    .attr({
                        x: 1000,
                        y: 50,
                    })
                    .text(cutoff.toFixed(2))

function render(c) {
    c.attr({
        cx: function(d) { return d.x },
        cy: function(d) { return d.y },
        r: 2,
        class: function(d) { return 'node_' + d.node }
    })
    .on("mouseover", function(d){highlightNodes(d.node)})
    .on("mouseout", function(){unhighlightNodes()})
    .append("title")
    .text(function(d) { return "node:" + d.node + "\nsample:" + d.sample + "\ni:" + d.i + "\nx:" + d.x + "\ny:" + d.y })
}

d3.select("body").on("keydown", function() {
    if (d3.event.keyCode == 38) { //up arrow
        if ( cutoff <= 0.99 ) { cutoff += 0.01 }
    } else if ( d3.event.keyCode == 40) { //down arrow
        if ( cutoff >= 0.01 ) { cutoff -= 0.01 }
    }
    cutoffText.text(cutoff.toFixed(2))
    var circles = svg.selectAll('circle').data(data.map(function(d) { return extractDataPoints(d,cutoff)}).reduce(function(a,b) { return a.concat(b)}))
    circles.exit().remove()
    circles.enter().append("circle")
    render(circles)
})


var scaleX = d3.scale.linear()
                .range([10, nrPixelsPerIndividual-10])
                .domain([d3.min(data, function(d) {
                  return d.x;
                }), d3.max(data, function(d) {
                  return d.x;
                })]);
var scaleY = d3.scale.linear()
                .range([10, nrPixelsPerIndividual-10])
                .domain([d3.min(data, function(d) {
                  return d.y;
                }), d3.max(data, function(d) {
                  return d.y;
                })]);

var extractDataPoints = function(d,cutoff) {
    var returnData = []
    d.values.forEach(function(dp,i) {
        if ( dp > cutoff ) {
            var x = scaleX(d.x) + nrPixelsPerIndividual*(i%nrColumns)
            var y = scaleY(d.y) + nrPixelsPerIndividual*(Math.floor(i/nrColumns))
            returnData.push({node:d.node,
                             x:x,
                             y:y,
                             sample:sampleNames[i],
                             i:i,
                             value:dp
                            })
        }
    })
    return returnData
}

var highlightNodes = function(nodeId) {
    svg.selectAll(".node_" + nodeId)
        .attr({r:6})
}
var unhighlightNodes = function() {
    svg.selectAll("circle").attr({r:2})
}

svg.selectAll("rect")
    .data([0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17])
    .enter()
    .append('rect')
    .attr({
        x: function(d) { return d%nrColumns*nrPixelsPerIndividual },
        y: function(d) { return Math.floor(d/nrColumns)*nrPixelsPerIndividual },
        width: nrPixelsPerIndividual-1,
        height: nrPixelsPerIndividual-1,
        fill: 'lightgrey'
    })
svg.selectAll("text.sampleNames")
    .data(sampleNames)
    .enter()
    .append("text")
    .attr({
        x: function(d,i) { return i%nrColumns*nrPixelsPerIndividual },
        y: function(d,i) { return Math.floor(i/nrColumns)*nrPixelsPerIndividual + 10},
        "font-family": 'sans-serif',
        'font-size': '11px',
        fill: 'black',
        class: "sampleNames"
    })
    .text(function(d) { return d })

var c = svg.selectAll("circle")
    .data(data.map(function(d) { return extractDataPoints(d,cutoff)}).reduce(function(a,b) { return a.concat(b)}))
    .enter().append("circle")
render(c)
